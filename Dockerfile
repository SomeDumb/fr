FROM python:3.10

WORKDIR /app/
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1
# Install Python dependencies
COPY ./requirements.txt /app/
RUN pip install --upgrade pip 
RUN pip install -r requirements.txt

# Add the rest of the code
COPY . /app/

# Collect static files
RUN mkdir /app/staticfiles

WORKDIR /app/

RUN python manage.py collectstatic --noinput

RUN python manage.py makemigrations --no-input
RUN python manage.py migrate --no-input
