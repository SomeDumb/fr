from django.contrib import admin
from .models import Client, Tag, Mailing, Message, OperatorCode, Filter


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    readonly_fields = ["operator_code"]


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass


@admin.register(OperatorCode)
class OperatorCodeAdmin(admin.ModelAdmin):
    pass


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
    pass


@admin.register(Filter)
class FilterAdmin(admin.ModelAdmin):
    pass
