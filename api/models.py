from django.db import models
from django.db.models.signals import post_save, pre_save
from django.conf import settings
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.core.validators import RegexValidator
import re
import pytz


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class Status(models.Model):
    text = models.CharField(max_length=20)

    def __str__(self):
        return self.text


class Tag(models.Model):
    text = models.CharField(max_length=100)

    def __str__(self):
        return self.text


class OperatorCode(models.Model):

    code = models.CharField(max_length=3)

    def __str__(self):
        return self.code


class Filter(models.Model):
    tags = models.ManyToManyField(Tag)
    codes = models.ManyToManyField(OperatorCode)

    def __str__(self):
        return ', '.join(self.tags)


class Mailing(models.Model):
    start_time = models.DateTimeField(auto_now=True, blank=True)
    text = models.TextField()
    client_filter = models.ForeignKey(Filter, on_delete=models.PROTECT)
    end_time = models.DateTimeField(null=True, blank=True)


class Client(models.Model):
    """ todo make phone field unique """
    phone_regex = RegexValidator(
        regex=r'(^7)((\d{10})|(\s\(\d{3}\)\s\d{3}\s\d{2}\s\d{2}))',
        message="Phone number must be entered in the format: '79999999999'. Up to 10 digits allowed.")
    code_regex = r'[^7]{3}'
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_number = models.CharField(validators=[phone_regex],
                                    max_length=11)
    tags = models.ManyToManyField(Tag, blank=True)
    timezone = models.CharField(max_length=32, choices=TIMEZONES,
                                default='UTC')
    operator_code = models.ForeignKey(
        OperatorCode, on_delete=models.PROTECT, blank=True)

    def get_operator_code(self):
        code = re.search("[^7]{3}", self.phone_number)[0]
        return code

    def save(self, *args, **kwargs):

        self.operator_code, created = OperatorCode.objects.get_or_create(
            code=self.get_operator_code())
        super().save(*args, **kwargs)

    def __str__(self):
        return self.phone_number



class Message(models.Model):
    send_time = models.DateTimeField(auto_now_add=True, blank=True)
    status = models.ForeignKey(Status, on_delete=models.CASCADE)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
