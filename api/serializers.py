from rest_framework import serializers
from .models import Client, Mailing, Tag, OperatorCode, Filter


class TagSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag
        fields = '__all__'


class OperatorCodeSerializer(serializers.ModelSerializer):

    class Meta:
        model = OperatorCode
        fields = '__all__'


class ClientSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)
    code = serializers.ReadOnlyField(source='operator_code.code')

    class Meta:
        model = Client
        fields = '__all__'
        read_only_fields = ['id']

    def create(self, validated_data):
        client_tags = validated_data.pop("tags", [])

        client_instance = Client.objects.create(**validated_data)
        for tag in client_tags:
            tag_instance, created = Tag.objects.get_or_create(**tag)
            client_instance.tags.add(tag_instance)
        return client_instance

    def update(self, instance, validated_data):
        instance.phone_number = validated_data.get(
            'phone_number', instance.phone_number)
        instance.timezone = validated_data.get('timezone', instance.timezone)

        tags = validated_data.pop('tags')

        for tag in tags:
            tag_instance, created = Tag.objects.get_or_create(**tag)
            instance.tags.add(tag_instance)
            instance.save()

        return instance


class FilterSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)

    class Meta:
        model = Filter
        fields = '__all__'

    def create(self, validated_data):
        client_tags = validated_data.pop("tags", [])
        codes = validated_data.pop('codes', [])

        filter_instance = Filter.objects.create(**validated_data)

        for code in codes:
            code_instance, created = OperatorCode.objects.get_or_create(code=code)
            filter_instance.codes.add(code_instance)

        for tag in client_tags:
            tag_instance, created = Tag.objects.get_or_create(**tag)
            filter_instance.tags.add(tag_instance)
        return filter_instance

    def update(self, instance, validated_data):

        tags = validated_data.pop('tags')
        codes = validated_data.pop('codes')

        for code in codes:
            code_instance, created = OperatorCode.objects.get_or_create(code=code)
            instance.codes.add(code_instance)

        for tag in tags:
            tag_instance, created = Tag.objects.get_or_create(**tag)
            instance.tags.add(tag_instance)
            instance.save()

        return instance


class MailingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mailing
        fields = '__all__'
