from django.urls import path, include
from django.contrib.auth.models import User
from rest_framework import routers
from .views import ClientView, MailingView, FilterView

router = routers.SimpleRouter()
router.register(r'clients', ClientView)
urlpatterns = router.urls

router = routers.SimpleRouter()
router.register(r'mailing', MailingView)
urlpatterns += router.urls

router = routers.SimpleRouter()
router.register(r'filters', FilterView)
urlpatterns += router.urls
